/**
 * Created by Bhavya on 4/4/16.
 */

var app = angular.module('myapp',[]);
app.controller('myCtrl', function($scope, $http, $sce){
    $scope.pageTitle="Gif Videos";
    $http.get(YOURAPIURL).
    success(function(data){
        $scope.contentInput = data.gfycats;
    });

});

app.directive('player', ['$sce', function ($sce) {
    'use strict';
    return {
        restrict: 'E',
        scope: {
            videos: '='
        },
        link: function (scope, element, attrs) {
            //element.addClass('player');
            scope.playing = false;
            scope.trustSrc = function(src) {
                return $sce.trustAsResourceUrl(src);
            }

            scope.play = function (index) {
                var video = element.find('video');
                video[index].play();
                scope.playing = true;
            };
            scope.pause = function(index){
                var video = element.find('video');
                video[index].pause();
                scope.playing = false;
            }
        },
        template:  '<div class="displayVideos" ng-repeat="item in videos">' +
        '<video  height="250px" width="300px" preload="none" poster="{{ trustSrc(item.posterUrl) }}" ng-mouseenter="play($index)" ng-mouseleave="pause($index)" loop>' +
        '<source ng-src="{{ trustSrc(item.mp4Url) }}" type="video/mp4" />' +
        '</video>'+ '</div>'

    };
}]);
